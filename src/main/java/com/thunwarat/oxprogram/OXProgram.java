/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.oxprogram;

import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class OXProgram {

    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'O';
    static Scanner kb = new Scanner(System.in);
    static int row, col;
    static boolean finish = false;
    static int count = 0;

    public static void main(String[] arge) {
        showWelcome();
        while (true) {
            showTable(table);
            showTurn();
            inputRowCol();
            process();
            if (finish) {
                break;
            }
        }
    }

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void showTurn() {
        System.out.println("Turn " + currentPlayer);
    }

    public static void inputRowCol() {
        row = kb.nextInt();
        col = kb.nextInt();
    }

    public static void showTable(char[][] table) {
        for (int r = 0; r < table.length; r++) {
            for (int c = 0; c < table[r].length; c++) {
                System.out.print(table[r][c]);
            }
            System.out.println();
        }
    }

    public static void process() {
        System.out.println();
        setTable();
        if (checkWin()) {
            finish = true;
            showWin();
            return;
        }
        count++;
        if (checkDraw()) {
            finish = true;
            showDraw();
        }
        switchPlayer();

    }

    public static void showWin() {
        showTable(table);
        System.out.println(">>> " + currentPlayer + " Win<<<");
    }

    private static void setTable() {
        table[row - 1][col - 1] = currentPlayer;
    }

    private static void switchPlayer() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X';
        } else {
            currentPlayer = 'O';
        }
    }

    public static boolean checkWin() {
        if (checkVertical()) {
            return true;
        } else if (checkHorizontal()) {
            return true;
        } else if (checkDiagonal()) {
            return true;
        }
        return false;
    }

    public static boolean checkVertical() {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkHorizontal() {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkDiagonal() {
        if (checkX1()) {
            return true;
        } else if (checkX2()) {
            return true;
        }
        return false;
    }

    public static boolean checkX1() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkX2() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkDraw() {
        if (count == 9) {
            return true;
        }
        return false;
    }

    public static void showDraw() {
        showTable(table);
        System.out.println(">>>Draw<<<");
    }

}
